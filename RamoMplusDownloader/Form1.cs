﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RamoMplusDownloader
{
    public partial class form1 : Form
    {
        public form1()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            string mpluschar = "Rambohans";
            string mplusserver = "Ravencrest";
            string mplusurl = "https://raider.io/api/characters/eu/" + mplusserver + "/" + mpluschar + "?season=season-7.3.0&tier=20";
            using (var webClient = new System.Net.WebClient())
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                try
                {
                    var mplusjson = webClient.DownloadString(mplusurl);
                    dynamic mplusraider = JObject.Parse(mplusjson);

                    string mplusilvleq = mplusraider.characterDetails.itemDetails.item_level_equipped;
                    string mplusscore = mplusraider.characterDetails.mythicPlusScores.all.score + 1;

                    label2.Text = mplusscore.Truncate(4);
                    string path = @"c:\Rambohans Mythic Score Downloader\mplus.txt";
                    if (File.Exists(path))
                    {
                        using (var tw = new StreamWriter(path, false))
                        {
                            tw.WriteLine(label2.Text);
                            tw.Close();
                        }
                    }
                }
                catch (Exception e404)
                {
                   
                    DirectoryInfo di = Directory.CreateDirectory(@"C:\Rambohans Mythic Score Downloader\");
                    string filePath = @"C:\Rambohans Mythic Score Downloader\Raiderio lookup user.txt";

                    using (StreamWriter writer = new StreamWriter(filePath, true))
                    {
                        writer.WriteLine("Message :" + e404.Message + "<br/>" + Environment.NewLine + "StackTrace :" + e404.StackTrace +
                           "" + Environment.NewLine + "Date :" + DateTime.Now.ToString() + Environment.NewLine + Environment.NewLine + "comamnd : " + mplusserver + " - " + mpluschar + Environment.NewLine + Environment.NewLine + "url: " + mplusurl);
                        writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (button1.Text == "Start Hansebassen ultra extreme m+ downloader")
            {
                timer1.Start();
                button1.Text = "Stop Hansebassen ultra extreme m+ downloader";
                timer1_Tick(null, EventArgs.Empty);
            }
            else
            {
                timer1.Stop();
                button1.Text = "Start Hansebassen ultra extreme m+ downloader";
            }

        }

        private void form1_Load(object sender, EventArgs e)
        {
            DirectoryInfo di = Directory.CreateDirectory(@"C:\Rambohans Mythic Score Downloader");
            string path = @"c:\Rambohans Mythic Score Downloader\mplus.txt";
            using (File.Create(path));
            if (File.Exists(path))
            {
                using (var tw = new StreamWriter(path, false))
                {
                    tw.WriteLine(label2.Text);
                    tw.Close();
                }
            }
        }
    }
    public static class StringExt
    {
        public static string Truncate(this string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }
    }
}
